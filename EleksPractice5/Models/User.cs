﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice5.Models
{
    public class User
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }

        public string WorkPlace { get; set; }

        public User(string firstName, string lastName, int age, string workPlace)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            WorkPlace = workPlace;
        }

        public User()
        {

        }
    }
}
