﻿using EleksPractice5.Abstract;
using EleksPractice5.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EleksPractice5.Infrastructure
{
    public class FileWorkHandler : IFileWorkHandler
    {
        private readonly ICsvSerializator serializator;

        public FileWorkHandler(ICsvSerializator csvSerializator)
        {
            serializator = csvSerializator;
        }

        public string SetFilePath()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = ".csv";
            if (dialog.ShowDialog() != DialogResult.OK)
            {
                throw new Exception("File problem!");
            }

            string path = dialog.FileName;
            Settings.SingletonObject.FilePath = path;
            return path;
        }

        public void AddToFile(User user)
        {
            using (StreamWriter file = new StreamWriter(Settings.SingletonObject.FilePath, true))
            {
                file.Write(serializator.ObjectToCsv(user));
                file.Close();
            }
        }
    }
}
