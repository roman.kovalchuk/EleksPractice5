﻿using EleksPractice5.Abstract;
using EleksPractice5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice5.Infrastructure
{
    public class CsvSerializator : ICsvSerializator
    {
        public string ObjectToCsv(User user)
        {
            string csv = $"{user.FirstName},{user.LastName},{user.Age},{user.WorkPlace}\n";
            return csv;
        }
    }
}
