﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice5.Infrastructure
{
    public class Settings
    {
        public static Settings SingletonObject { get; private set; } = new Settings();

        public string FilePath
        {
            get
            {
                return Properties.Settings.Default.FilePath;
            }
            set
            {
                Properties.Settings.Default.FilePath = value;
            }
        }

        private Settings()
        {

        }
    }
}
