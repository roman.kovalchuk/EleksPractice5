﻿using EleksPractice5.Abstract;
using EleksPractice5.Infrastructure;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EleksPractice5
{
    class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<ICsvSerializator>().To<CsvSerializator>();
            Bind<IFileWorkHandler>().To<FileWorkHandler>();
            Bind<Form>().To<HomeForm>();
        }
    }
}
