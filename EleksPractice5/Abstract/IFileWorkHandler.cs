﻿using EleksPractice5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice5.Abstract
{
    public interface IFileWorkHandler
    {
        string SetFilePath();
        void AddToFile(User user);
    }
}
