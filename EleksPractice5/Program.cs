﻿using EleksPractice5.Abstract;
using Ninject;
using System;
using System.Reflection;
using System.Windows.Forms;

namespace EleksPractice5
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var kernel = new StandardKernel();
            kernel.Load(new Bindings());
            var form = kernel.Get<Form>();

            Application.Run(form);
        }
    }
}
