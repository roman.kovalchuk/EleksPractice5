﻿using EleksPractice5.Abstract;
using EleksPractice5.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EleksPractice5
{
    public partial class HomeForm : Form
    {
        private readonly IFileWorkHandler fileWorkHandler;

        public HomeForm(IFileWorkHandler fileWorkHandler)
        {
            InitializeComponent();
            this.fileWorkHandler = fileWorkHandler;
        }

        private void buttonAddToFile_Click(object sender, EventArgs e)
        {
            try
            {
                fileWorkHandler.AddToFile(new Models.User(textBoxFirstName.Text, textBoxLastName.Text,
                    int.Parse(textBoxAge.Text), textBoxWorkPlace.Text));
                richTextBoxHistory.Text += $"Added {textBoxFirstName.Text} {textBoxLastName.Text} at {DateTime.Now.Hour}:{DateTime.Now.Minute}\n";
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void buttonChooseFile_Click(object sender, EventArgs e)
        {
            try
            {
                labelFileName.Text = fileWorkHandler.SetFilePath();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }
    }
}
