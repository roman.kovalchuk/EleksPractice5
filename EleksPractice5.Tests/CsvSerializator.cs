﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using EleksPractice5.Abstract;
using EleksPractice5.Models;
using EleksPractice5.Infrastructure;

namespace EleksPractice5.Tests
{
    [TestClass]
    public class CsvSerializator
    {
        [TestMethod]
        public void ObjectToCsv_RomanKovalchuk19Lviv_RomanKovalchuk19Lviv()
        {
            // Arrange       
            Mock<ICsvSerializator> serializator = new Mock<ICsvSerializator>();
            User user = new User("Roman", "Kovalchuk", 18, "Lviv");
            string expectedResult = "Roman,Kovalchuk,18,Lviv\n";
            TestHelper helper = new TestHelper();
            
            // Act 
            serializator.Setup(x => x.ObjectToCsv(user)).Returns(expectedResult);
            string result = helper.GetCsv(serializator.Object, user);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }
    }
}
