﻿using EleksPractice5.Abstract;
using EleksPractice5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EleksPractice5.Tests
{
    class TestHelper
    {
        public string GetCsv(ICsvSerializator serializator, User user)
        {
            return serializator.ObjectToCsv(user);
        }
    }
}
